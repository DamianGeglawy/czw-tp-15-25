
package zad_serwer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import static zad_serwer.Serwer.numer_gracza;
import static zad_serwer.Serwer.Ktora_kratka;
import static zad_serwer.Serwer.tura;
import static zad_serwer.Serwer.win;

public class OdKlienta implements Runnable {

	Socket gracz1;
	Socket gracz2;
	int nr;

	public OdKlienta(Socket gracz_in) {
		gracz1 = gracz_in;
		nr = numer_gracza;
		numer_gracza++;
	}

	void tura_gracza2(Socket gracz2_in) {
		gracz2 = gracz2_in;
	}

	@Override
	public void run() {
		BufferedReader in = null;
		PrintWriter out = null;
		PrintWriter gracz2_wyj = null;
		try {
			in = new BufferedReader(new InputStreamReader(gracz1.getInputStream()));
			out = new PrintWriter(gracz1.getOutputStream(), true);
			gracz2_wyj = new PrintWriter(gracz2.getOutputStream(), true);
			out.println(nr);
		} catch (IOException e) {
		}
		String info;
		int gdzie_jest;
		while (true) {
			try {
				info = in.readLine();
				if (Integer.parseInt(info.substring(0, 1)) == tura) {
					gdzie_jest = Integer.parseInt(info.substring(1, 2));
					if (nr == 1) {
						Ktora_kratka[gdzie_jest] = 1;
					} else {
						Ktora_kratka[gdzie_jest] = 2;
					}
					out.println(info);
					gracz2_wyj.println(info);
					if (tura == 1)
						tura = 2;
					else if (tura == 2)
						tura = 1;
				}

			} catch (IOException e) {

			}

			if (kto_wygral()) {
				if (win == 1) {
					out.println(19);
					gracz2_wyj.println(19);
				} else {
					out.println(29);
					gracz2_wyj.println(29);

				}
				for (int i = 0; i < 9; i++) {
					Ktora_kratka[i] = 0;
				}
			}
		}
	}

	boolean kto_wygral() {
		if ((Ktora_kratka[0] == 1 && Ktora_kratka[1] == 1 && Ktora_kratka[2] == 1)
				|| (Ktora_kratka[3] == 1 && Ktora_kratka[4] == 1 && Ktora_kratka[5] == 1)
				|| (Ktora_kratka[6] == 1 && Ktora_kratka[7] == 1 && Ktora_kratka[8] == 1)
				|| (Ktora_kratka[0] == 1 && Ktora_kratka[3] == 1 && Ktora_kratka[6] == 1)
				|| (Ktora_kratka[1] == 1 && Ktora_kratka[4] == 1 && Ktora_kratka[7] == 1)
				|| (Ktora_kratka[2] == 1 && Ktora_kratka[5] == 1 && Ktora_kratka[8] == 1)
				|| (Ktora_kratka[0] == 1 && Ktora_kratka[4] == 1 && Ktora_kratka[8] == 1)
				|| (Ktora_kratka[6] == 1 && Ktora_kratka[4] == 1 && Ktora_kratka[2] == 1)) {
			win = 1;
			return true;
		} else if ((Ktora_kratka[0] == 2 && Ktora_kratka[1] == 2 && Ktora_kratka[2] == 2)
				|| (Ktora_kratka[3] == 2 && Ktora_kratka[4] == 2 && Ktora_kratka[5] == 2)
				|| (Ktora_kratka[6] == 2 && Ktora_kratka[7] == 2 && Ktora_kratka[8] == 2)
				|| (Ktora_kratka[0] == 2 && Ktora_kratka[3] == 2 && Ktora_kratka[6] == 2)
				|| (Ktora_kratka[1] == 2 && Ktora_kratka[4] == 2 && Ktora_kratka[7] == 2)
				|| (Ktora_kratka[2] == 2 && Ktora_kratka[5] == 2 && Ktora_kratka[8] == 2)
				|| (Ktora_kratka[0] == 2 && Ktora_kratka[4] == 2 && Ktora_kratka[8] == 2)
				|| (Ktora_kratka[6] == 2 && Ktora_kratka[4] == 2 && Ktora_kratka[2] == 2)) {
			win = 2;
			return true;
		}
		return false;
	}

}
