

package zad_serwer;

import java.io.IOException;
import java.net.ServerSocket;
public class Serwer {
    private ServerSocket serwer;
    
    static int[] Ktora_kratka;
    static int tura = 1;
    static int win;
    static int numer_gracza;
    
    
    
    public Serwer() {
        numer_gracza=1;
        try {
        inicjuj_serwer();
        rozpocznij_gre();
        }
        catch(IOException e) {
            
        }
    }
    
    private void inicjuj_serwer() throws IOException{
        
        serwer = new ServerSocket(666);
        OdKlienta k1;
        OdKlienta k2;
        try {
            System.out.println("gracz 1 nie jest zalogowany");
            k1 = new OdKlienta(serwer.accept());
            System.out.println("gracz 2 nie jest zalogowany");
            k2 = new OdKlienta(serwer.accept());
            k1.tura_gracza2(k2.gracz1);
            k2.tura_gracza2(k1.gracz1);
            Thread watek1 = new Thread(k1);
            Thread watek2 = new Thread(k2);
            watek1.start();
            watek2.start();
        }
        catch(IOException e) {
        }
        
    }
    
    private void rozpocznij_gre() {
        Ktora_kratka = new int[9];
        for(int i=0; i<9; i++) {
            Ktora_kratka[i]=0;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        serwer.close();
        super.finalize();
    }
}
