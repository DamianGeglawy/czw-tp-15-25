
package zad_klient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Wygr_i_Rekolor implements Runnable {
	String nummer;
	BufferedReader wej;
	Plansza Plansza;

	public Wygr_i_Rekolor(Socket klient, Plansza p) {
		Plansza = p;
		try {
			wej = new BufferedReader(new InputStreamReader(klient.getInputStream()));
			nummer = wej.readLine();
			Plansza.nr = nummer;
		} catch (IOException ex) {

		}
	}

	@Override
	public void run() {
		String linia;
		while (true) {
			try {
				linia = wej.readLine();
				if (linia.charAt(1) == '9') { 
					if (linia.charAt(0) == nummer.charAt(0)) {
						System.out.println("wygral gracz 1");
						System.exit(-1);

					} else
						System.out.println("wygral gracz 2");
					System.exit(-1);
				} else {
					if (linia.charAt(0) == nummer.charAt(0)) {
						Plansza.Rekolorr(linia.charAt(1), '1');
					} else {
						Plansza.Rekolorr(linia.charAt(1), '2');
					}
				}

			} catch (IOException ex) {

			}

		}
	}

}
