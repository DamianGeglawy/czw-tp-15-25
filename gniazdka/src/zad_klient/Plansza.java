
package zad_klient;

import java.awt.Color;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.MaskFormatter;

public class Plansza extends javax.swing.JFrame {

	Klient klient;
	String nr;

	public Plansza() {
		klient = new Klient();
		Wyglad_planszy();
		sprawdzaj();

	}

	private void Wyglad_planszy() {

		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		try {
			jFormattedTextField1 = new javax.swing.JFormattedTextField(new MaskFormatter("###.###.###.###"));
			jFormattedTextField2 = new javax.swing.JFormattedTextField();
			jButton1 = new javax.swing.JButton();
			jPanel3 = new javax.swing.JPanel();
			pole0 = new javax.swing.JPanel();
			pole1 = new javax.swing.JPanel();
			pole2 = new javax.swing.JPanel();
			pole3 = new javax.swing.JPanel();
			pole4 = new javax.swing.JPanel();
			pole5 = new javax.swing.JPanel();
			pole6 = new javax.swing.JPanel();
			pole7 = new javax.swing.JPanel();
			pole8 = new javax.swing.JPanel();
			setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
			jLabel1.setText("IP         ");
			jLabel2.setText("Port      ");

		} catch (ParseException e) {

		}

		jButton1.setText("polacz");
		jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

			}
		});

		jPanel3.setLayout(new java.awt.GridLayout(3, 3));

		pole0.setBackground(new java.awt.Color(255, 255, 255));
		pole0.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 0, 0), 5, true));
		pole0.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				pole0klik(evt);
			}
		});

		javax.swing.GroupLayout pole0Layout = new javax.swing.GroupLayout(pole0);
		pole0.setLayout(pole0Layout);
		pole0Layout.setHorizontalGroup(pole0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 138, Short.MAX_VALUE));
		pole0Layout.setVerticalGroup(pole0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 128, Short.MAX_VALUE));

		jPanel3.add(pole0);

		pole1.setBackground(new java.awt.Color(255, 255, 255));
		pole1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 0, 0), 5, true));
		pole1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				pole1klik(evt);
			}
		});

		javax.swing.GroupLayout pole1Layout = new javax.swing.GroupLayout(pole1);
		pole1.setLayout(pole1Layout);
		pole1Layout.setHorizontalGroup(pole1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 138, Short.MAX_VALUE));
		pole1Layout.setVerticalGroup(pole1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 128, Short.MAX_VALUE));

		jPanel3.add(pole1);

		pole2.setBackground(new java.awt.Color(255, 255, 255));
		pole2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 0, 0), 5, true));
		pole2.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				pole2klik(evt);
			}
		});

		javax.swing.GroupLayout pole2Layout = new javax.swing.GroupLayout(pole2);
		pole2.setLayout(pole2Layout);
		pole2Layout.setHorizontalGroup(pole2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 138, Short.MAX_VALUE));
		pole2Layout.setVerticalGroup(pole2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 128, Short.MAX_VALUE));

		jPanel3.add(pole2);

		pole3.setBackground(new java.awt.Color(255, 255, 255));
		pole3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 0, 0), 5, true));
		pole3.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				pole3klik(evt);
			}
		});

		javax.swing.GroupLayout pole3Layout = new javax.swing.GroupLayout(pole3);
		pole3.setLayout(pole3Layout);
		pole3Layout.setHorizontalGroup(pole3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 138, Short.MAX_VALUE));
		pole3Layout.setVerticalGroup(pole3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 128, Short.MAX_VALUE));

		jPanel3.add(pole3);

		pole4.setBackground(new java.awt.Color(255, 255, 255));
		pole4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 0, 0), 5, true));
		pole4.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				pole4klik(evt);
			}
		});

		javax.swing.GroupLayout pole4Layout = new javax.swing.GroupLayout(pole4);
		pole4.setLayout(pole4Layout);
		pole4Layout.setHorizontalGroup(pole4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 138, Short.MAX_VALUE));
		pole4Layout.setVerticalGroup(pole4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 128, Short.MAX_VALUE));

		jPanel3.add(pole4);

		pole5.setBackground(new java.awt.Color(255, 255, 255));
		pole5.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 0, 0), 5, true));
		pole5.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				pole5klik(evt);
			}
		});

		javax.swing.GroupLayout pole5Layout = new javax.swing.GroupLayout(pole5);
		pole5.setLayout(pole5Layout);
		pole5Layout.setHorizontalGroup(pole5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 138, Short.MAX_VALUE));
		pole5Layout.setVerticalGroup(pole5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 128, Short.MAX_VALUE));

		jPanel3.add(pole5);

		pole6.setBackground(new java.awt.Color(255, 255, 255));
		pole6.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 0, 0), 5, true));
		pole6.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				pole6klik(evt);
			}
		});

		javax.swing.GroupLayout pole6Layout = new javax.swing.GroupLayout(pole6);
		pole6.setLayout(pole6Layout);
		pole6Layout.setHorizontalGroup(pole6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 138, Short.MAX_VALUE));
		pole6Layout.setVerticalGroup(pole6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 128, Short.MAX_VALUE));

		jPanel3.add(pole6);

		pole7.setBackground(new java.awt.Color(255, 255, 255));
		pole7.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 0, 0), 5, true));
		pole7.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				pole7klik(evt);
			}
		});

		javax.swing.GroupLayout pole7Layout = new javax.swing.GroupLayout(pole7);
		pole7.setLayout(pole7Layout);
		pole7Layout.setHorizontalGroup(pole7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 138, Short.MAX_VALUE));
		pole7Layout.setVerticalGroup(pole7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 128, Short.MAX_VALUE));

		jPanel3.add(pole7);

		pole8.setBackground(new java.awt.Color(255, 255, 255));
		pole8.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 0, 0), 5, true));
		pole8.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				pole8klik(evt);
			}
		});

		javax.swing.GroupLayout pole8Layout = new javax.swing.GroupLayout(pole8);
		pole8.setLayout(pole8Layout);
		pole8Layout.setHorizontalGroup(pole8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 138, Short.MAX_VALUE));
		pole8Layout.setVerticalGroup(pole8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 128, Short.MAX_VALUE));

		jPanel3.add(pole8);
		// dostosowanie sie planszy ze stacka
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup().addContainerGap()
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addComponent(jLabel1)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jFormattedTextField1, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGroup(layout.createSequentialGroup().addComponent(jLabel2)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(
										jFormattedTextField2, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addComponent(jButton1))
				.addContainerGap(461, Short.MAX_VALUE))
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
						javax.swing.GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup().addContainerGap(232, Short.MAX_VALUE).addComponent(jPanel3,
								javax.swing.GroupLayout.PREFERRED_SIZE, 428, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addContainerGap())));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup().addContainerGap()
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jLabel1)
						.addComponent(jFormattedTextField1, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(6, 6, 6)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jLabel2)
						.addComponent(jFormattedTextField2, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jButton1)
				.addContainerGap(309, Short.MAX_VALUE))
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))));

		jFormattedTextField1.setText("xxx.xxx.xxx.xxx");
		jFormattedTextField2.setText("xxxx");

		pack();
	}

	private void pole0klik(java.awt.event.MouseEvent evt) {
		String ktora_linia = new String();
		ktora_linia = nr + "0";
		klient.wyjscie.println(ktora_linia);
	}

	private void pole1klik(java.awt.event.MouseEvent evt) {
		String linia = new String();
		linia = nr + "1";
		klient.wyjscie.println(linia);
	}

	private void pole2klik(java.awt.event.MouseEvent evt) {
		String linia = new String();
		linia = nr + "2";
		klient.wyjscie.println(linia);
	}

	private void pole3klik(java.awt.event.MouseEvent evt) {
		String linia = new String();
		linia = nr + "3";
		klient.wyjscie.println(linia);
	}

	private void pole4klik(java.awt.event.MouseEvent evt) {
		String linia = new String();
		linia = nr + "4";
		klient.wyjscie.println(linia);
	}

	private void pole5klik(java.awt.event.MouseEvent evt) {
		String linia = new String();
		linia = nr + "5";
		klient.wyjscie.println(linia);
	}

	private void pole6klik(java.awt.event.MouseEvent evt) {
		String linia = new String();
		linia = nr + "6";
		klient.wyjscie.println(linia);
	}

	private void pole7klik(java.awt.event.MouseEvent evt) {
		String linia = new String();
		linia = nr + "7";
		klient.wyjscie.println(linia);
	}

	private void pole8klik(java.awt.event.MouseEvent evt) {
		String linia = new String();
		linia = nr + "8";
		klient.wyjscie.println(linia);
	}



	public static void main(String args[]) {

		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Plansza().setVisible(true);
			}
		});
	}

	public void Rekolorr(char pole, char gracz) {
		switch (pole) {
		case '0':
			if (gracz == '1')
				pole0.setBackground(Color.blue);
			else
				pole0.setBackground(Color.orange);
			break;
		case '1':
			if (gracz == '1')
				pole1.setBackground(Color.blue);
			else
				pole1.setBackground(Color.orange);
			break;
		case '2':
			if (gracz == '1')
				pole2.setBackground(Color.blue);
			else
				pole2.setBackground(Color.orange);
			break;
		case '3':
			if (gracz == '1')
				pole3.setBackground(Color.blue);
			else
				pole3.setBackground(Color.orange);
			break;
		case '4':
			if (gracz == '1')
				pole4.setBackground(Color.blue);
			else
				pole4.setBackground(Color.orange);
			break;
		case '5':
			if (gracz == '1')
				pole5.setBackground(Color.blue);
			else
				pole5.setBackground(Color.orange);
			break;
		case '6':
			if (gracz == '1')
				pole6.setBackground(Color.blue);
			else
				pole6.setBackground(Color.orange);
			break;
		case '7':
			if (gracz == '1')
				pole7.setBackground(Color.blue);
			else
				pole7.setBackground(Color.orange);
			break;
		case '8':
			if (gracz == '1')
				pole8.setBackground(Color.blue);
			else
				pole8.setBackground(Color.orange);
			break;
		}
	}

	void sprawdzaj() {
		Wygr_i_Rekolor spr;
		spr = new Wygr_i_Rekolor(klient.klient, this);
		Thread w = new Thread(spr);
		w.start();
	}

	private javax.swing.JButton jButton1;	private javax.swing.JFormattedTextField jFormattedTextField1;
	private javax.swing.JFormattedTextField jFormattedTextField2;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JPanel jPanel3;
	private javax.swing.JPanel pole0;
	private javax.swing.JPanel pole1;
	private javax.swing.JPanel pole2;
	private javax.swing.JPanel pole3;
	private javax.swing.JPanel pole4;
	private javax.swing.JPanel pole5;
	private javax.swing.JPanel pole6;
	private javax.swing.JPanel pole7;
	private javax.swing.JPanel pole8;
}
